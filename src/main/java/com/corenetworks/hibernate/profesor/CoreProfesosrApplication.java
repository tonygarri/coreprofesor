package com.corenetworks.hibernate.profesor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreProfesosrApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreProfesosrApplication.class, args);
	}
}
