package com.corenetworks.hibernate.profesor.beans;

public class PostExamBean {

	private long id;
	private String pregunta;
	private String respuesta;
	
	
	public PostExamBean() {
		id=-1;
	}


	
	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getPregunta() {
		return pregunta;
	}


	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}


	public String getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}


	
	
}
