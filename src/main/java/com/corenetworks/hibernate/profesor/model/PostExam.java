package com.corenetworks.hibernate.profesor.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;




@Entity
@Table(name="examenes")
public class PostExam {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@Column
	private String pregunta;
	@Column
	private String respuesta;
	@ManyToOne
	private UserProfesor profesor;
	@Column
	@CreationTimestamp
	private Date fecha;
	
	
	public PostExam() {
		super();
	}



	public PostExam(String pregunta, String respuesta) {
		super();
		this.pregunta = pregunta;
		this.respuesta = respuesta;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public UserProfesor getProfesor() {
		return profesor;
	}

	public void setProfesor(UserProfesor profesor) {
		this.profesor = profesor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	
	
	
	
	
	
	
}