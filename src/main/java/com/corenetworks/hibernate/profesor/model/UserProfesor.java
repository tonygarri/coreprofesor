package com.corenetworks.hibernate.profesor.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;



@Entity
public class UserProfesor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@Column(name="NAME")
	private String nombre;
	@Column
	private String ciudad;
	@Column
	private String instituto;
	@Column
	@CreationTimestamp
	private Date fechaAlta;
	@Column
	private String email;
	@Column
	@ColumnTransformer(write=" MD5(?) ")
	private String password;
	
	
	public UserProfesor() {
		super();
	}

	public UserProfesor(String nombre, String ciudad, String instituto, String email, String password) {
		super();
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.instituto = instituto;
		this.email = email;
		this.password = password;
	}

	
	public Set<PostExam> getPosts() {
		return posts;
	}

	public void setPosts(Set<PostExam> posts) {
		this.posts = posts;
	}

	@OneToMany(mappedBy="profesor", fetch= FetchType.EAGER)	
	private Set<PostExam> posts= new HashSet<>();	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getInstituto() {
		return instituto;
	}

	public void setInstituto(String instituto) {
		this.instituto = instituto;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
	@Override
	public String toString() {
		return "User [id=" + id + ", nombre=" + nombre + ", ciudad=" + ciudad + ", instituto=" + instituto
				+ ", fechaAlta=" + fechaAlta + ", email=" + email + ", password=" + password + "]";
	}

	
	
	
	
	

}
