package com.corenetworks.hibernate.profesor.dao;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.profesor.model.UserProfesor;



@Repository
@Transactional
public class UserProfesorDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	/*
	 * Almacena el usuario en la base de datos
	 */
	public void create(UserProfesor usuario) {
		entityManager.persist(usuario);
	}
	
	@SuppressWarnings("unchecked")
	public List<UserProfesor> getAll(){
		return entityManager
				.createQuery("Select u from UserProfesor u")
				.getResultList();
	}

	
	/*
	 * Validar login a partir de email y password
	 */

	public UserProfesor getByEmailAndPassword(String email, String password) {
		UserProfesor resultado = null;
		try {
		resultado = (UserProfesor) entityManager.createNativeQuery("select * FROM userprofesor where email= :email and password=md5(:password)", UserProfesor.class)
		.setParameter("email", email)
		.setParameter("password", password)
		.getSingleResult();
		} catch(NoResultException e) {
			resultado = null;
		}
		return resultado;
		
	}
	
}