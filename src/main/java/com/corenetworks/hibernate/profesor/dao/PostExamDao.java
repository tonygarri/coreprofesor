package com.corenetworks.hibernate.profesor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.corenetworks.hibernate.profesor.model.PostExam;


@Repository
@Transactional
public class PostExamDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el post en la base de datos
     */
    public void create(PostExam post) {
    	entityManager.persist(post);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<PostExam> getAll(){
    	return entityManager
    			.createQuery("select p from PostExam p")
    			.getResultList();
    }

	public PostExam getById(long id) {
		// TODO Auto-generated method stub
		return entityManager.find(PostExam.class, id);
	}

	

	public void update(PostExam postExam) {
		// TODO Auto-generated method stub
		entityManager.merge(postExam);
		
	}

	public void delete(PostExam postExam) {
		// TODO Auto-generated method stub
		entityManager.remove(postExam);
	}
    
    
 
    
    
    
}