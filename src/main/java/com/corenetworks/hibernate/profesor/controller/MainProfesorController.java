package com.corenetworks.hibernate.profesor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.profesor.dao.PostExamDao;


@Controller
public class MainProfesorController {
	
	@Autowired
	private PostExamDao postDao;
	
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		
		modelo.addAttribute("listaPost", postDao.getAll());
		return "index";
		
	}
}

