package com.corenetworks.hibernate.profesor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.corenetworks.hibernate.profesor.beans.RegisterBean;
import com.corenetworks.hibernate.profesor.dao.UserProfesorDao;
import com.corenetworks.hibernate.profesor.model.UserProfesor;

@Controller
public class RegisterProfesorController {
	
	@Autowired
	private UserProfesorDao userProfesorDao;
	
	
	@GetMapping(value = "/signup")
	public String showForm(Model model) {
		
		model.addAttribute("userRegister", new RegisterBean());

		return "register";
		
	}

	@PostMapping(value = "/register")
	public String submit(@ModelAttribute("userRegister") RegisterBean r, Model model) {

		userProfesorDao.create(new UserProfesor(r.getNombre(), r.getCiudad(), r.getInstituto(), r.getEmail(), r.getPassword())); 
		
		return "redirect:/profesores";
	}	

}
