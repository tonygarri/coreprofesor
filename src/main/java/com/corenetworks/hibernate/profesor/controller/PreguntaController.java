package com.corenetworks.hibernate.profesor.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.corenetworks.hibernate.profesor.beans.RegisterBean;
import com.corenetworks.hibernate.profesor.beans.PostExamBean;
import com.corenetworks.hibernate.profesor.dao.PostExamDao;
import com.corenetworks.hibernate.profesor.model.PostExam;
import com.corenetworks.hibernate.profesor.model.UserProfesor;

@Controller
public class PreguntaController {
	@Autowired
	private PostExamDao postExamDao;

	// @Autowired
	// private CommentDao commentDao;

	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("post", new PostExamBean());
		return "submit";
	}

	@GetMapping(value = "/submit/{id}")
	public String modifyForm(@PathVariable("id") long id, Model modelo) {
		PostExam result = null;
		if ((result = postExamDao.getById(id)) != null) {
			PostExamBean p = new PostExamBean();
			p.setPregunta(result.getPregunta());
			p.setRespuesta(result.getRespuesta());
			modelo.addAttribute("post", p);
			return "submit";
		} else
			return "redirect:/";

	}

	@PostMapping(value = "/submit/newpost")
	public String submit(@ModelAttribute("post") PostExamBean postBean, Model model) {
		// userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(),
		// r.getPassword()));
		// crear un Post
		// Obtener el autor desde la sesión
		// Asignar el autor al post
		// Hacer persistente el post
		// PostExam postExam = new PostExam();
		// postExam.setPregunta(postBean.getPregunta());
		// postExam.setRespuesta(postBean.getRespuesta());

		PostExam postExam;

		if (postBean.getId() > 0) {

			postExam = postExamDao.getById(postBean.getId());

		} else {
			postExam = new PostExam();
			postExam.setPregunta(postBean.getPregunta());
			postExam.setRespuesta(postBean.getRespuesta());
		}

		UserProfesor profesor = (UserProfesor) httpSession.getAttribute("userLoggedIn");
		postExam.setProfesor(profesor);
		;

		if (postBean.getId() < 0) 
			postExamDao.create(postExam);
		 
		else 	
			postExamDao.update(postExam);		
		// postExamDao.create(postExam);
		profesor.getPosts().add(postExam);
		 
		
		
		
			return "redirect:/";
	}

	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id") long id, Model modelo) {
		// modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.

		PostExam result = null;
		if ((result = postExamDao.getById(id)) != null) {
			modelo.addAttribute("post", result);
			// modelo.addAttribute("commentForm", new CommentBean());
			return "examendetail";
		} else
			return "redirect:/";
	}

	
	
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") long id, Model modelo) {
		    
		    PostExam result = null;
		    
		if ((result = postExamDao.getById(id)) != null) {
			postExamDao.delete(result);
		}
			
		return "redirect:/";

	}


}
