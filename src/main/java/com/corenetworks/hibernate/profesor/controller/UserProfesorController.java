package com.corenetworks.hibernate.profesor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.corenetworks.hibernate.profesor.dao.UserProfesorDao;


@Controller
public class UserProfesorController {
	
	@Autowired
	private UserProfesorDao userProfesorDao;
	
	@GetMapping(value = "/profesores")
	public String listaAutores(Model model) {

		model.addAttribute("profesores", userProfesorDao.getAll());

		return "userprofesorlist";
	}

}
