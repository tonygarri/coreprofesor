<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description"
	content="Blog de profesores para hacer preguntas y respuestas.">

<title>Blog AskQuestion Exams</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css"  rel="stylesheet"> 
 <link href="assets/css/registrar.css"  rel="stylesheet">

</head>
<body>



	<div class="container">
		<nav class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand">AskQuestion Exam</a>
		</div>
		<ul class="nav nav-pills pull-right">
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn }">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="inicio" name="inicio" />
						<jsp:param name="usuario"
							value="${sessionScope.userLoggedIn.nombre }" />

					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="inicio" name="inicio" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
		</ul>

		</nav>
		<link
			href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
			rel="stylesheet"
			integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
			crossorigin="anonymous">
			
		<div class="text-center">
			<div class="space-medium">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="account-holder">
							<h3>Regístrate como profesor</h3>
							<br>
							<div class="social-btn">
								<h6>!Síguenos en nuestras redes!</h6>
								<div class="fb-btn">
									<i class="fa fa-facebook-official"></i><a href="#"
										class="fb-btn-text">facebook</a>
								</div>
								<div class="google-btn">
									<i class="fa fa-google"></i><a href="#" class="google-btn-text">Google</a>
								</div>
							</div>


							<form:form id="register-form" action="/register" method="post"
								role="form" autocomplete="off" modelAttribute="userRegister">
								<div class="form-group">
									<form:input type="text" name="nombre" id="nombre" tabIndex="1"
										class="form-control" path="nombre" placeholder="Nombre" />
								</div>
								<div class="form-group">
									<form:input type="text" name="cuidad" id="ciudad" tabIndex="2"
										class="form-control" path="ciudad" placeholder="Cuidad, País" />
								</div>
								<div class="form-group">
									<form:input type="text" name="instituto" id="instituto"
										tabIndex="3" class="form-control" path="instituto"
										placeholder="Instituto" />
								</div>
								<div class="form-group">
									<form:input type="email" name="email" id="email" tabIndex="4"
										class="form-control" path="email" placeholder="Email" />
								</div>
								<div class="form-group">
									<form:input type="password" name="password" id="password"
										tabIndex="5" class="form-control" path="password"
										placeholder="Password" />
								</div>
								<div class="form-group">
									<input type="password" name="confirm-password"
										id="confirm-password" tabIndex="6" class="form-control"
										placeholder="Repetir Password" data-rule-equalTo="#password" />

								</div>
								<div class="form-group">
									<input type="submit" name="register-submit"
										id="register-submit" tabIndex="7"
										class="form-control btn btn-info" value="Registrar ahora" />

								</div>
							</form:form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>



	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/messages_es.js"></script>
	<script>
		$(document).ready(function() {
			$("#register-form").validate();
		});
	</script>
</body>
</html>