
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description"
	content="Blog de profesores para hacer preguntas y respuestas.">

<title>Blog AskQuestion Exams</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/css/profile.css" rel="stylesheet">
<link href="assets/css/registrar.css"  rel="stylesheet">

</head>
<body>



	<div class="container">
		<nav class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand">AskQuestion Exam</a>
		</div>
		<ul class="nav nav-pills pull-right">
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn }">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="login" name="login" />
						<jsp:param name="usuario"
							value="${sessionScope.userLoggedIn.nombre }" />

					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="login" name="login" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
		</ul>

		</nav>
		
		<link
			href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
			rel="stylesheet"
			integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
			crossorigin="anonymous">
			
		<div class="row">

			<div class="col-lg-8 col-lg-offset-2">
			
			

				<div class="text-center">
					<h3>LOGUEATE COMO PROFESOR</h3>
					<br>
					<br>
							<div class="social-btn">
								<h6>!Síguenos en nuestras redes!</h6>
								<div class="fb-btn">
									<i class="fa fa-facebook-official"></i><a href="#"
										class="fb-btn-text">facebook</a>
								</div>
								<div class="google-btn">
									<i class="fa fa-google"></i><a href="#" class="google-btn-text">Google</a>
								</div>
							</div>
					<form:form id="login-form" action="/login" method="post"
						role="form" modelAttribute="userLogin">
						<div class="form-group">
							<form:input type="email" name="email" id="email" tabIndex="1"
								class="form-control" path="email" placeholder="Email"
								required="required" autofocus="autofocus" />
						</div>
						<div class="form-group">
							<form:input type="password" name="password" id="password"
								tabIndex="2" class="form-control" path="password"
								placeholder="Password" required="required" />
						</div>
						<div class="form-group">
							<button type="submit" name="login-submit" id="login-submit"
								tabIndex="3" class="btn btn-lg btn-primary btn-block">Login</button>
						</div>
					</form:form>
				</div>
			</div>
		</div>




		<c:if test="${not empty error}">
			<div class="row">

				<div class="col-lg-8 col-lg-offset-2">
					<div class="alert alert-danger alert-dismissible fade in"
						role="alert">
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<!--  Esto hace que se muestre una X en la zona de error  -->
							<%
								// Esto hace que se muestre una X en la zona de error
							%>
							<%
								/* Esto hace que se muestre una X en la zona de error 
										            Varias líneas
										          
										     */
							%>

							<span aria-hidden="true">&times;</span>
						</button>
						<c:out value="${error}"></c:out>
					</div>
				</div>
			</div>
		</c:if>
		<footer class="footer"> </footer>



	</div>


	<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/messages_es.js"></script>
	<script>
		$(document).ready(function() {
			$("#register-form").validate();
		});
	</script>
	</div>
</body>
</html>
