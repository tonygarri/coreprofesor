<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description"
	content="Blog de profesores para hacer preguntas y respuestas.">

<title>Blog AskQuestion Exams</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css" rel="stylesheet">
</head>
<body>



	<div class="container">
		<nav class="navbar navbar-inverse" >
		<div class="navbar-header">
			<a class="navbar-brand">AskQuestion Exam</a>
		</div>
		<ul class="nav nav-pills pull-right">
			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn }">
					<jsp:include page="includes/menu_logged.jsp">
						<jsp:param value="inicio" name="inicio" />
						<jsp:param name="usuario"
							value="${sessionScope.userLoggedIn.nombre }" />

					</jsp:include>
				</c:when>
				<c:otherwise>
					<jsp:include page="includes/menu.jsp">
						<jsp:param value="inicio" name="inicio" />
					</jsp:include>
				</c:otherwise>
			</c:choose>
		</ul>

		</nav>

		<div class="jumbotron"
			style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">

			<div class="card">

				<div
					class="text-white text-center d-flex align-items-center py-5 px-4 my-5">
					<div>
						<h1>AskQuestion Exams</h1>
						<h3 class="card-title pt-3 mb-5 font-bold">
							<strong>Formula preguntas y respuestas como profesor
								para tus alumnos.</strong>
						</h3>
						<p class="mx-5 mb-5">Lorem ipsum dolor sit amet, consectetur
							adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio
							vero odio nam sit officia accusamus minus error nisi architecto
							nulla ipsum dignissimos. Odit sed qui, dolorum!.</p>


						<c:if test="${empty sessionScope.userLoggedIn }">
							<p>
								<a href="/signup" role="button" class="btn btn-2 btn-success ">Regístrate</a>
							</p>
						</c:if>
					</div>
				</div>
			</div>
		</div>


		<c:forEach items="${listaPost}" var="postItem">
			<link rel="stylesheet"
				href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" />

			<div class="well">
				<div class="media">
					<a class="pull-left" href="http://i.pravatar.cc"> <img
						class="media-object"
						src="http://i.pravatar.cc/180x180?u=${postItem.profesor.email}">
					</a>
					<div class="media-body">
						<p class="text-right">Profesor ${postItem.profesor.nombre}</p>
						
						<p><h4> Pregunta </h4></p>
						<p>${postItem.pregunta}</p>
						<p><h4> Respuesta </h4></p>
						<p>${postItem.respuesta}</p>
						
						<p> <b>Instituto:</b> ${postItem.profesor.instituto}</p>
						<p> <b>Email:</b> ${postItem.profesor.email}</p>
						<ul class="list-inline list-unstyled">
							<li><a class="btn btn-default" href="/post/${postItem.id}">Read
									More</a></li>
							<li>|</li>

							<li><span><i class="glyphicon glyphicon-calendar"></i>
									${postItem.fecha} </span></li>
							<li>|</li>
							<li><a class="btn btn-default" href="/submit/${postItem.id}">Modificar</a></li>
							<li>|</li>
							<li><a class="btn btn-default" href="/delete/${postItem.id}">Borrar</a></li>
							<li>|</li>
					
					
							<li>
								<!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
								<a href="https://www.facebook.com" target="_blank"><img
									alt="siguenos en facebook" height="32"
									src="http://2.bp.blogspot.com/-q_Tm1PpPfHo/UiXnJo5l-VI/AAAAAAAABzU/MKdrVYZjF0c/s1600/face.png"
									title="siguenos en facebook" width="32" /></a> <a
								href="https://accounts.google.com" target="_blank"><img
									alt="siguenos en Google+" height="32"
									src="http://1.bp.blogspot.com/-1W0m8yLdJtU/UidI-7j2BnI/AAAAAAAAB-Y/2wH5M_ZpXO8/s1600/plusone.png"
									title="siguenos en Google+" width="32" /></a> <a
								href="https://twitter.com" target="_blank"><img
									alt="siguenos en Twitter" height="32"
									src="http://3.bp.blogspot.com/-wlwaJJG-eOY/UiXnHS2jLsI/AAAAAAAAByQ/I2tLyZDLNL4/s1600/Twitter+NEW.png"
									title="siguenos en Twitter" width="32" /></a> <a
								href="https://www.blog.google/" target="_blank"><img
									alt="sígueme en Blogger" height="32"
									src="http://1.bp.blogspot.com/-_NWymh6-9I4/UiXnEcZ1UMI/AAAAAAAABw4/UkFXkztSeOY/s1600/Google+Blogger.png"
									title="sígueme en Blogger" width="32" /></a>



							</li>
						</ul>
					</div>
					</div>
					
					</div>
					

					</c:forEach>




	</div>
</body>
</html>



