<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Blog Curso Java Cloud</title>

<!-- Bootstrap core CSS -->
<link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">


<!-- Custom styles for this template -->

<link href="<c:url value="/assets/css/jumbotron-narrow.css" />"
	rel="stylesheet">
<link href="<c:url value="/assets/css/submitform.css" />"
	rel="stylesheet">

<link
	href="<c:url value="/assets/wysiwyg/bootstrap3-wysihtml5.min.css" />"
	rel="stylesheet">
 <link href="assets/css/registrar.css"  rel="stylesheet">
 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">
		<nav class="navbar navbar-inverse">
			<div class="navbar-header">
				<a class="navbar-brand">AskQuestion Exam</a>
			</div>
			<ul class="nav nav-pills pull-right">
				<c:choose>
					<c:when test="${not empty sessionScope.userLoggedIn }">
						<jsp:include page="includes/menu_logged.jsp">
							<jsp:param value="submit" name="submit" />
							<jsp:param name="usuario"
								value="${sessionScope.userLoggedIn.nombre }" />

						</jsp:include>
					</c:when>
					<c:otherwise>
						<jsp:include page="includes/menu.jsp">
							<jsp:param value="submit" name="submit" />
						</jsp:include>
					</c:otherwise>
				</c:choose>
			</ul>

		</nav>


		<div class="row">
			<form:form method="POST" modelAttribute="post"
				action="/submit/newpost" role="form" id="contact-form"
				class="contact-form">
				
				

				<div class="text-center">
					<div class="space-medium">
						<div class="row">
							<div class="col-lg-8 col-lg-offset-2">
								<div class="account-holder">
									<h3>Formula tu pregunta</h3>
									<br>
									
								<!--	 <div class="social-btn">
										<h6>!Síguenos en nuestras redes!</h6>
										<div class="fb-btn">
											<i class="fa fa-facebook-official"></i><a href="#"
												class="fb-btn-text">facebook</a>
										</div>
										<div class="google-btn">
											<i class="fa fa-google"></i><a href="#"
												class="google-btn-text">Google</a>
										</div>
									</div> -->



									<div class="col-md-6">
										<div class="form-group">
											<form:input type="text" class="form-control" name="pregunta"
												autocomplete="off" id="pregunta" placeholder="Pregunta"
												path="pregunta" />
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<form:input type="text" class="form-control" name="respuesta"
												autocomplete="off" id="respuesta" placeholder="Respuesta"
												path="Respuesta" />
										</div>
									</div>

									<div class="row">
									
										<div class="col-md-6">
											<button type="submit" class="btn btn-default">Aceptar</button>
										</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</form:form>
		</div>
	

	<footer class="footer"> </footer>

	</div>
	<!-- /container -->
	
	<script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>

</body>


</html>
