 
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Blog Curso Java Cloud</title>

<!-- Bootstrap core CSS -->
<link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">


<!-- Custom styles for this template -->

<link href="<c:url value="/assets/css/jumbotron-narrow.css" />"
	rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
</head>

<body>

	<div class="container">
		<nav class="navbar navbar-inverse">
			<div class="navbar-header">
				<a class="navbar-brand">AskQuestion Exam</a>
			</div>
			<ul class="nav nav-pills pull-right">
				<c:choose>
					<c:when test="${not empty sessionScope.userLoggedIn }">
						<jsp:include page="includes/menu_logged.jsp">
							<jsp:param value="inicio" name="inicio" />
							<jsp:param name="usuario"
								value="${sessionScope.userLoggedIn.nombre }" />

						</jsp:include>
					</c:when>
					<c:otherwise>
						<jsp:include page="includes/menu.jsp">
							<jsp:param value="otro" name="otro" />
						</jsp:include>
					</c:otherwise>
				</c:choose>
			</ul>
		</nav>
		
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<h1>Pregunta de Examen #${post.id}</h1>
				<div>
					<div class="pull-right" style="padding: 10px 0 0 5px;">${post.profesor.nombre}</div>
					<img alt="User Pic"
						src="http://i.pravatar.cc/50?u=${post.profesor.email}"
						class="img-circle img-responsive pull-right">
					<p></p>
				</div>
				<div style="clear: both; margin-bottom: 10px;"></div>
				<h4>Pregunta</h4>
				<p>${post.pregunta}</p>
				<p></p>
				<h4>Respuesta</h4>
				<p>${post.respuesta}</p>
				<div>
				
					<span class="badge">Escrito el <fmt:formatDate
							pattern="dd/MM/yyyy" value="${post.fecha}" /> a las <fmt:formatDate
							pattern="HH:mm:ss" value="${post.fecha}" /></span>
		
			</div>
		</div>



</body>
</html>